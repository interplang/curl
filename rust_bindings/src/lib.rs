#![allow(non_snake_case)]
#![allow(unused_variables)]

use interplang::vm::value::Value;
use interplang::parser::loc::Loc;
use interplang::parser::loc::get_line;

use std::sync::Mutex;
use std::io::Cursor;
use std::io::Read;

use curl::easy::Easy;

lazy_static::lazy_static! {
    static ref CONNECTIONS: Mutex<Vec<curl::easy::Easy>> = Mutex::new(vec![]);
}

#[no_mangle]
pub fn Interp_Curl_Init(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    let mut easy = Easy::new();
    let mut con_lock = CONNECTIONS.lock().unwrap();
    con_lock.push(easy);
    Value::Int((con_lock.len() - 1) as i64)
}

#[no_mangle]
pub fn Interp_Curl_SetUrl(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    let Value::Int(id) = args[0].0.clone() else {
        panic!();
    };
    let mut con_lock = CONNECTIONS.lock().unwrap();
    if con_lock.len() as i64 <= id || id < 0 {
        let loc = get_line(&args[0].1);
        eprintln!("interp: curl: {}:{}:{}: invalid connection", args[0].1.filename, loc.1, loc.0);
        return Value::Null;
    }
    let id = id as usize;
    let Value::String(url) = args[1].0.clone() else {
        panic!();
    };
    con_lock[id].url(&url).unwrap();
    Value::Void
}

#[no_mangle]
pub fn Interp_Curl_SetMethod(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    let Value::Int(id) = args[0].0.clone() else {
        panic!();
    };
    let Value::String(method) = args[1].0.clone() else {
        panic!();
    };
    let mut con_lock = CONNECTIONS.lock().unwrap();
    if con_lock.len() as i64 <= id || id < 0 {
        let loc = get_line(&args[0].1);
        eprintln!("interp: curl: {}:{}:{}: invalid connection", args[0].1.filename, loc.1, loc.0);
        return Value::Null;
    }
    let id = id as usize;
    if method == "get" {
        con_lock[id].get(true).unwrap();
    } else if method == "post" {
        con_lock[id].post(true).unwrap();
    } else if method == "put" {
        con_lock[id].put(true).unwrap();
    }
    Value::Void
}

#[no_mangle]
pub fn Interp_Curl_SetHeaders(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    let Value::Int(id) = args[0].0.clone() else {
        panic!();
    };
    let Value::List(headers) = args[1].0.clone() else {
        panic!();
    };
    let mut con_lock = CONNECTIONS.lock().unwrap();
    if con_lock.len() as i64 <= id || id < 0 {
        let loc = get_line(&args[0].1);
        eprintln!("interp: curl: {}:{}:{}: invalid connection", args[0].1.filename, loc.1, loc.0);
        return Value::Null;
    }
    let id = id as usize;
    let mut list = curl::easy::List::new();
    for header in headers {
        let Value::String(header) = header else {
            panic!();
        };
        list.append(&header);
    }
    con_lock[id].http_headers(list);
    Value::Void
}

#[no_mangle]
pub fn Interp_Curl_RequestString(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    let Value::Int(id) = args[0].0.clone() else {
        panic!();
    };
    let Value::String(body) = args[1].0.clone() else {
        panic!();
    };
    let Value::Boolean(include_header) = args[2].0.clone() else {
        panic!();
    };
    let mut con_lock = CONNECTIONS.lock().unwrap();
    if con_lock.len() as i64 <= id || id < 0 {
        let loc = get_line(&args[0].1);
        eprintln!("interp: curl: {}:{}:{}: invalid connection", args[0].1.filename, loc.1, loc.0);
        return Value::Null;
    }
    let id = id as usize;

    con_lock[id].show_header(include_header).unwrap();

    let mut cursor = Cursor::new(body);
    let mut output = vec![];

    {
        let mut transfer = con_lock[id].transfer();
        transfer.read_function(|buf| {
            Ok(cursor.read(buf).unwrap())
        });
        transfer.write_function(|buf| {
            output.extend_from_slice(buf);
            Ok(buf.len())
        });
        match transfer.perform() {
            Ok(()) => {}
            Err(err) => return Value::List(vec![Value::Int(err.code() as i64), Value::String(err.description().to_owned())]),
        }
    }


    Value::String(String::from_utf8_lossy(&output).into_owned())
}

#[no_mangle]
pub fn Interp_Curl_RequestBuffer(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    let Value::Int(id) = args[0].0.clone() else {
        panic!();
    };
    let Value::ByteVec(body) = args[1].0.clone() else {
        panic!();
    };
    let Value::Boolean(include_header) = args[2].0.clone() else {
        panic!();
    };
    let mut con_lock = CONNECTIONS.lock().unwrap();
    if con_lock.len() as i64 <= id || id < 0 {
        let loc = get_line(&args[0].1);
        eprintln!("interp: curl: {}:{}:{}: invalid connection", args[0].1.filename, loc.1, loc.0);
        return Value::Null;
    }
    let id = id as usize;

    con_lock[id].show_header(include_header).unwrap();

    let mut cursor = Cursor::new(body);
    let mut output = vec![];

    {
        let mut transfer = con_lock[id].transfer();
        transfer.read_function(|buf| {
            Ok(cursor.read(buf).unwrap())
        });
        transfer.write_function(|mut buf| {
            Ok(buf.read(&mut output).unwrap())
        });
        match transfer.perform() {
            Ok(()) => {}
            Err(err) => return Value::List(vec![Value::Int(err.code() as i64), Value::String(err.description().to_owned())]),
        }
    }
    Value::ByteVec(output)
}
