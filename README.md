# curl
Wrapper around the excellent [curl](https://crates.io/crates/curl) crate.

# Packi Package
Packi package 'curl' available [here](https://packi.kriikkula.net/curl).
