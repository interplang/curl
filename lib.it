imp "@packi/lib.it";
# packi::Require("oak", "0.0.2");
imp "@oak-1.0.0/lib.it";

var private_foreign_functions = [
    ImportForeign(packi::BINDINGS_LIB, "Curl_Init"),
    ImportForeign(packi::BINDINGS_LIB, "Curl_SetUrl"),
    ImportForeign(packi::BINDINGS_LIB, "Curl_SetMethod"),
    ImportForeign(packi::BINDINGS_LIB, "Curl_SetHeaders"),
    ImportForeign(packi::BINDINGS_LIB, "Curl_RequestString"),
    ImportForeign(packi::BINDINGS_LIB, "Curl_RequestBuffer"),
];

atom GET;
atom POST;

enum Method {
    GET,
    POST,
}

`<doc>
Represents a libcurl error.
</doc>`
struct Error {
    code Int[1..],
    message String,
}

`<doc>
A libcurcl easy connection.
</doc>`
struct Connection {
    id Int[0..],
    set_url Function<Connection, String, Void>,
    set_method Function<Connection, Method, Void>,
    set_headers Function<Connection, List<String>, Void>,
    request_string Function<Connection, String or Null, Bool, String or Error>,
    request_buffer Function<Connection, Buffer or Null, Bool, Buffer or Error>
}

`<doc>
Initialize a connection.
</doc>`
fun Init() Connection {
    fun Method_SetUrl(connection Connection, url String) {
        private_foreign_functions[1](connection.id, url);
    }
    fun Method_SetMethod(connection Connection, method Method) {
        if method == GET {
            method = "get";
        } else if method == POST {
            method = "post";
        }
        private_foreign_functions[2](connection.id, method);
    }
    fun Method_SetHeaders(connection Connection, headers List<String>) {
        private_foreign_functions[3](connection.id, headers);
    }
    fun Method_RequestString(connection Connection, body String or Null = null, include_header Bool = false) String or Error {
        if body == null {
            body = "";
        }
        var err_or_response = private_foreign_functions[4](connection.id, body, include_header);
        if err_or_response is List {
            ret {
                code = err_or_response[0],
                message = err_or_response[1],
            };
        }
        ret err_or_response;
    }
    fun Method_RequestBuffer(connection Connection, body Buffer or Null = null, include_header Bool = false) Buffer or Error {
        if body == null {
            body = ToBytes("");
        }
        var err_or_response = private_foreign_functions[5](connection.id, body, include_header);
        if err_or_response is List {
            ret {
                code = err_or_response[0],
                message = err_or_response[1],
            };
        }
        ret err_or_response;
    }
    ret {
        id = private_foreign_functions[0](),
        set_url = Method_SetUrl,
        set_method = Method_SetMethod,
        set_headers = Method_SetHeaders,
        request_string = Method_RequestString,
        request_buffer = Method_RequestBuffer,
    };
}
